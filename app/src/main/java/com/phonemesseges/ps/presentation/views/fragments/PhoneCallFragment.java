package com.phonemesseges.ps.presentation.views.fragments;

import android.arch.paging.PagedList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.repository.SharedPrefferences.CallRepository;
import com.phonemesseges.ps.domain.interactor.InteractorInterface;
import com.phonemesseges.ps.domain.interactor.PhoneCallInteractor;
import com.phonemesseges.ps.domain.repository.ICallRepository;
import com.phonemesseges.ps.presentation.contract.PhoneCallContract;
import com.phonemesseges.ps.presentation.presenters.PhoneCallPresenter;
import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;
import com.phonemesseges.ps.presentation.views.adapters.RecyclerViewAdapter;
import com.phonemesseges.ps.presentation.views.adapters.paging.MainThreadExecutor;
import com.phonemesseges.ps.presentation.views.adapters.paging.PhoneLogPositionalDataSource;
import com.phonemesseges.ps.presentation.views.adapters.paging.PagingRVAdapter;
import com.phonemesseges.ps.presentation.views.dialogs.CommentariesDialog;
import com.phonemesseges.ps.service.App;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.Executors;

public class PhoneCallFragment extends Fragment implements PhoneCallContract.View, PagingRVAdapter.DialogListener {
    RecyclerView recyclerView;
    RecyclerViewAdapter rVAdapter;

    PagingRVAdapter pagingRVAdapter;

    PhoneCallContract.Presenter presenter;

    ICallRepository repository;
    InteractorInterface interactor;

    ArrayList<Model> models;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new PhoneCallPresenter();
        presenter.attachView(this);

        View view = inflater.inflate(R.layout.fragment_phonecall, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);

        repository = new CallRepository(getContext());
        interactor = new PhoneCallInteractor(repository);

        presenter.setInteractor(interactor);
        presenter.getCallLog();

        setRV();

        return view;
    }

    private void setRV() {
        PhoneLogPositionalDataSource dataSource = new PhoneLogPositionalDataSource(new CallRepository(getContext()), ((App)getActivity().getApplication()).getWebRepository());

        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(15)
                .build();

        PagedList pagedList = new PagedList.Builder<>(dataSource, config)
                .setNotifyExecutor(new MainThreadExecutor())
                .setFetchExecutor(Executors.newFixedThreadPool(2))
                .build();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        pagingRVAdapter = new PagingRVAdapter(getContext());
        pagingRVAdapter.setDialogListener(this);
        pagingRVAdapter.submitList(pagedList);

        recyclerView.setAdapter(pagingRVAdapter);
    }

    @Override
    public void setBeginningData(ArrayList<Model> callLog) {
        models = callLog;
        Collections.reverse(models);
    }

    @Override
    public void onClickAdd(String phoneNumber) {
        CommentariesDialog commentariesDialog = new CommentariesDialog();

        Bundle bundle = new Bundle();
        bundle.putString("phoneNumber", phoneNumber);
        commentariesDialog.setArguments(bundle);

        commentariesDialog.show(getActivity().getSupportFragmentManager(), "commentaries dialog");
    }

    @Override
    public void onResume() {
        super.onResume();
        setRV();
    }
}
