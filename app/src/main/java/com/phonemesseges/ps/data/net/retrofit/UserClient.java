package com.phonemesseges.ps.data.net.retrofit;

import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse.GetCommentsResponse;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponseList.GetCommentsResponseList;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse.RegistrationResponse;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserClient {

    @FormUrlEncoded
    @POST("user-register")
    Observable<RegistrationResponse> numberRegistration(@Field("device_key") String deviceKey, @Field("phone") String phone);

    @FormUrlEncoded
    @POST("user-auth")
    Observable<RegistrationResponse> numberAuthorization(@Field("device_key") String deviceKey, @Field("phone") String phone);

    @FormUrlEncoded
    @POST("edit-phone-comment")
    Observable<RegistrationResponse> addComment(@Field("token_key") String tokenKey, @Field("phone") String phone, @Field("comment") String comment, @Field("status") int status);

    @FormUrlEncoded
    @POST("get-comment-list")
    Observable<GetCommentsResponse> getComments(@Field("token_key") String tokenKey, @Field("phone") String phone);

    @FormUrlEncoded
    @POST("get-last-comment-by-phones")
    Observable<GetCommentsResponseList> getLastCommentByPhones(@Field("token_key") String tokenKey, @Field("phones") String phones);

    @FormUrlEncoded
    @POST("delete-phone-comment")
    Observable<RegistrationResponse> deletePhoneComments(@Field("token_key") String tokenKey, @Field("phone") String phone);
}
