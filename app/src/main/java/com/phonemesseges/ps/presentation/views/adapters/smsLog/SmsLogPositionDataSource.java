package com.phonemesseges.ps.presentation.views.adapters.smsLog;

import android.annotation.SuppressLint;
import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;

import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponseList.GetCommentsResponseList;
import com.phonemesseges.ps.domain.repository.ISmsRepository;
import com.phonemesseges.ps.domain.repository.IWebRepository;
import com.phonemesseges.ps.presentation.presenters.model.Enum.SmsLogModel;
import com.phonemesseges.ps.presentation.presenters.model.Enum.SmsModel;
import com.phonemesseges.ps.utility.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class SmsLogPositionDataSource extends PositionalDataSource {
    private List<SmsLogModel> smsLog;

    private ISmsRepository smsRepository;
    private IWebRepository webRepository;

    boolean uniqueState;

    public SmsLogPositionDataSource(ISmsRepository smsRepository, IWebRepository webRepository) {
        this.smsRepository = smsRepository;
        this.webRepository = webRepository;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback callback) {
        smsRepository.getSmsLog().subscribe(new Observer<ArrayList<SmsLogModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<SmsLogModel> smsLogModels) {
                smsLog = new ArrayList<>();

                for (SmsLogModel item : smsLogModels) {
                    if (!smsLog.contains(item)) smsLog.add(item);
                }

                if (params.requestedLoadSize >= smsLog.size())
                    return;
                int endPosition = params.requestedStartPosition + params.requestedLoadSize;
                List<SmsLogModel> partOfSmsLog = smsLog.subList(params.requestedStartPosition, endPosition);


                loadPhoneData(callInfo -> callback.onResult(callInfo, 0), partOfSmsLog);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback callback) {
        if (params.startPosition >= smsLog.size())
            return;
        int endPosition = params.startPosition + params.loadSize;
        endPosition = endPosition >= smsLog.size() ? smsLog.size() - 1 : endPosition;
        List<SmsLogModel> partOfContacts = smsLog.subList(params.startPosition, endPosition);

        loadPhoneData(callInfo -> callback.onResult(callInfo), partOfContacts);
    }

    public interface CallBack {
        void setCallBack(List<SmsModel> smsModel);
    }

    @SuppressLint("CheckResult")
    private void loadPhoneData(SmsLogPositionDataSource.CallBack callBack, List<SmsLogModel> partOfSms) {
        Observable.fromArray(partOfSms)
                .flatMap(new Function<List<SmsLogModel>, Observable<GetCommentsResponseList>>() {
                             @Override
                             public Observable<GetCommentsResponseList> apply(List<SmsLogModel> listSms) throws Exception {
                                 List<String> numbers = new ArrayList<>();
                                 for (SmsLogModel item : listSms) {
                                     if (item.getContactName() != null && !item.getContactName().isEmpty())
                                         numbers.add(item.getContactName());
                                 }
                                 String[] array = numbers.toArray(new String[numbers.size()]);
                                 return webRepository.getLastCommentByPhones(Utility.getNumbersJson(array));
                             }
                         },
                        new BiFunction<List<SmsLogModel>, GetCommentsResponseList, List<SmsModel>>() {
                            @Override
                            public List<SmsModel> apply(List<SmsLogModel> smsLogModel, GetCommentsResponseList commentsResponse) throws Exception {
                                List<SmsModel> data = new ArrayList<>();
                                for (SmsLogModel item : smsLogModel) {
                                    SmsModel smsModel = new SmsModel();
                                    smsModel.setSmsText(item.getSmsText());
                                    smsModel.setName(item.getContactName());
                                    smsModel.setDate(item.getSmsDate());
                                    if (commentsResponse.data.comments == null || commentsResponse.data.comments.length == 0) {
                                        smsModel.setTitleText("");
                                    } else {
                                        String comment = null;
                                        for (int i = 0; i < commentsResponse.data.comments.length; i++) {
                                            if (item.getPhoneNumber().equals(commentsResponse.data.comments[i].phone)) {
                                                comment = commentsResponse.data.comments[i].comment;
                                            }
                                        }
                                        smsModel.setTitleText(comment);
                                    }
                                    data.add(smsModel);
                                }
                                return data;
                            }
                        })
                //.toSortedList(new SmsLogPositionDataSource.StringDateComparator())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(callBack::setCallBack, Throwable::printStackTrace);

    }

    class StringDateComparator implements Comparator<SmsModel> {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.getDefault());

        public int compare(SmsModel o1, SmsModel o2) {
            try {
                String data1 = o1.getDate().replace(" г.,", "");
                String data2 = o2.getDate().replace(" г.,", "");
                return -dateFormat.parse(data1).compareTo(dateFormat.parse(data2));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }
}
