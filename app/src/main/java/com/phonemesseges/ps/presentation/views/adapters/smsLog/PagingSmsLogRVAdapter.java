package com.phonemesseges.ps.presentation.views.adapters.smsLog;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.presentation.presenters.model.Enum.SmsModel;

public class PagingSmsLogRVAdapter extends PagedListAdapter<SmsModel, PagingSmsLogRVAdapter.SmsInfoHolder> {
    private Context context;

    public PagingSmsLogRVAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;

    }

    private static DiffUtil.ItemCallback<SmsModel> DIFF_CALLBACK = new DiffUtil.ItemCallback<SmsModel>() {
        @Override
        public boolean areItemsTheSame(@NonNull SmsModel oldItemPosition, @NonNull SmsModel newItemPosition) {
            return oldItemPosition.getSmsText().equals(newItemPosition.getSmsText());
        }

        @Override
        public boolean areContentsTheSame(@NonNull SmsModel oldItem, @NonNull SmsModel newItem) {
            return oldItem.equals(newItem);
        }
    };

    @NonNull
    @Override
    public SmsInfoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sms, viewGroup, false);
        return new SmsInfoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SmsInfoHolder smsInfoHolder, int i) {
        smsInfoHolder.name.setText(getItem(i).getName());
        smsInfoHolder.smsText.setText(getItem(i).getSmsText());
        smsInfoHolder.titleText.setText(getItem(i).getTitleText());
        smsInfoHolder.date.setText(getItem(i).getDate());
    }

    public class SmsInfoHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView smsText;
        TextView titleText;
        TextView date;

        public SmsInfoHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            smsText = itemView.findViewById(R.id.smsText);
            titleText = itemView.findViewById(R.id.titleText);
            date = itemView.findViewById(R.id.date);
        }
    }
}
