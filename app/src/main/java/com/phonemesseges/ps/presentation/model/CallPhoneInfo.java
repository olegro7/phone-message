package com.phonemesseges.ps.presentation.model;

public class CallPhoneInfo {
    private String phoneName;
    private String phoneNumber;
    private String phoneComment;
    private String date;

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneComment(String phoneComment) {
        this.phoneComment = phoneComment;
    }

    public String getPhoneComment() {
        return phoneComment;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
