package com.phonemesseges.ps.service;

import android.Manifest;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.phonemesseges.ps.data.repository.SharedPrefferences.WebRepository;
import com.phonemesseges.ps.domain.repository.IWebRepository;

public class App extends Application {
    public static final String CHANNEL_ID = "serviceChannel";

    private IWebRepository repository;

    @Override
    public void onCreate() {
        super.onCreate();
        repository = new WebRepository(this);

        createNotificationChannel();

    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    public IWebRepository getWebRepository() {
        return repository;
    }
}
