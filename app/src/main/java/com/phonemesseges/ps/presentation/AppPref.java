package com.phonemesseges.ps.presentation;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPref {
    private Context context;
    private SharedPreferences preferences;

    public AppPref(Context context) {
        this.context = context;
    }
    public boolean isCallNotification() {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean("call_notification", false);
    }
}
