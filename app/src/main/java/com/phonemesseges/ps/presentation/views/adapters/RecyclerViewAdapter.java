package com.phonemesseges.ps.presentation.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.presentation.model.DialogActivityModel;
import com.phonemesseges.ps.presentation.presenters.model.Enum.CommentariesModel;
import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;
import com.phonemesseges.ps.presentation.presenters.model.Enum.PhoneCallModel;
import com.phonemesseges.ps.presentation.presenters.model.Enum.SmsModel;
import com.phonemesseges.ps.presentation.views.activities.CommentariesActivity;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int phoneCallViewType = 1;
    private final int smsViewType = 2;
    private final int commentariesViewType = 3;
    private final int dialogViewType = 4;

    private Context context;
    private ArrayList<Model> models;

    private DialogListener dialogListener;

    public interface DialogListener {
        void onClickAdd(String phoneNumber);
    }

    public void setDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    public RecyclerViewAdapter(Context context, ArrayList<Model> models) {
        this.context = context;
        this.models = models;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view;
        switch (viewType){
            case phoneCallViewType:
                view = layoutInflater.inflate(R.layout.item_phonecall, viewGroup, false);
                return new MyViewHolderPhoneCall(view);
            case smsViewType:
                view = layoutInflater.inflate(R.layout.item_sms, viewGroup, false);
                return new MyViewHolderSms(view);
            case commentariesViewType:
                view = layoutInflater.inflate(R.layout.item_commentaries, viewGroup, false);
                return new MyViewHolderCommentaries(view);
            case dialogViewType:
                view = layoutInflater.inflate(R.layout.item_language, viewGroup, false);
                return new MyViewHolderDialogActivity(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Model model = models.get(position);
        if (model == null) return;
        switch (viewHolder.getItemViewType()) {
            case phoneCallViewType:
                onBindPhoneCallModel(model, viewHolder);
                break;
            case smsViewType:
                onBindSmsModel(model, viewHolder);
                break;
            case commentariesViewType:
                onBindCommentariesModel(model, viewHolder);
                break;
            case dialogViewType:
                onBindDialogActivityModel(model, viewHolder);
                break;
        }
    }

    private void onBindDialogActivityModel(Model model, RecyclerView.ViewHolder viewHolder) {
        MyViewHolderDialogActivity holder = (MyViewHolderDialogActivity) viewHolder;
        if (model instanceof DialogActivityModel) {
            DialogActivityModel dialogActivityModel = (DialogActivityModel) model;
            holder.imageRes.setImageResource(dialogActivityModel.getImageRes());
            holder.language.setText(dialogActivityModel.getLanguage());
            if (holder.radioBtn.isChecked()) {

            }
        }
    }

    private void onBindPhoneCallModel(Model model, RecyclerView.ViewHolder viewHolder) {
        MyViewHolderPhoneCall holder = (MyViewHolderPhoneCall) viewHolder;
        if (model instanceof PhoneCallModel) {
            PhoneCallModel phoneCallModel = (PhoneCallModel) model;
            holder.tvNumber.setText(phoneCallModel.getNumber());
            holder.tvTitleText.setVisibility(phoneCallModel.getTitleText() == null || phoneCallModel.getTitleText().isEmpty()? View.GONE : View.VISIBLE);
            holder.tvTitleText.setText(phoneCallModel.getTitleText());
            holder.tvDate.setText(phoneCallModel.getDate());
            holder.imageWarning.setImageResource(phoneCallModel.getWarning());
        }
    }

    private void onBindSmsModel(Model model, RecyclerView.ViewHolder viewHolder) {
        MyViewHolderSms holder = (MyViewHolderSms) viewHolder;
        if (model instanceof SmsModel) {
            SmsModel smsModel = (SmsModel) model;
            holder.tvNumber.setText(smsModel.getName());
            holder.tvSmsText.setText(smsModel.getSmsText());
            holder.tvTitleText.setText(smsModel.getTitleText());
            holder.tvDate.setText(smsModel.getDate());
            holder.imageWarning.setImageResource(smsModel.getWarning());
        }

    }

    private void onBindCommentariesModel(Model model, RecyclerView.ViewHolder viewHolder) {
        MyViewHolderCommentaries holder = (MyViewHolderCommentaries) viewHolder;
        if (model instanceof CommentariesModel) {
            CommentariesModel commentariesModel = (CommentariesModel) model;
            holder.tvDate.setText(String.valueOf(commentariesModel.getDate()));
            holder.tvText.setText(commentariesModel.getText());
        }
    }

    @Override
    public int getItemViewType(int position) {
             if (models.get(position) instanceof PhoneCallModel) return phoneCallViewType;
        else if (models.get(position) instanceof SmsModel) return smsViewType;
        else if (models.get(position) instanceof CommentariesModel) return commentariesViewType;
        else if (models.get(position) instanceof DialogActivityModel) return dialogViewType;
        return -1;
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class MyViewHolderPhoneCall extends RecyclerView.ViewHolder {
        TextView tvNumber;
        TextView tvTitleText;
        TextView tvDate;
        ImageView imageWarning;
        ImageButton btnAddCommentaries;

        public MyViewHolderPhoneCall(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, CommentariesActivity.class);
                intent.putExtra("phone", ((PhoneCallModel)models.get(getAdapterPosition())).getNumber());
                intent.putExtra("phoneDate", ((PhoneCallModel)models.get(getAdapterPosition())).getDate());
                context.startActivity(intent);
            });
            tvNumber = itemView.findViewById(R.id.name);
            tvTitleText = itemView.findViewById(R.id.titleText);
            tvDate = itemView.findViewById(R.id.date);
            imageWarning = itemView.findViewById(R.id.warning);
            btnAddCommentaries = itemView.findViewById(R.id.btnAddCommentaries);
            btnAddCommentaries.setOnClickListener(v -> dialogListener.onClickAdd(((PhoneCallModel)models.get(getAdapterPosition())).getNumber()));
        }
    }

    class MyViewHolderSms extends RecyclerView.ViewHolder {
        TextView tvNumber;
        TextView tvSmsText;
        TextView tvTitleText;
        TextView tvDate;
        ImageView imageWarning;

        public MyViewHolderSms(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {

            });
            tvNumber = itemView.findViewById(R.id.name);
            tvSmsText = itemView.findViewById(R.id.smsText);
            tvTitleText = itemView.findViewById(R.id.titleText);
            tvDate = itemView.findViewById(R.id.date);
            imageWarning = itemView.findViewById(R.id.warning);
        }
    }

    class MyViewHolderCommentaries extends RecyclerView.ViewHolder {
        TextView tvDate;
        TextView tvText;

        public MyViewHolderCommentaries(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {

            });
            tvDate = itemView.findViewById(R.id.date);
            tvText = itemView.findViewById(R.id.text);
        }
    }

    class MyViewHolderDialogActivity extends RecyclerView.ViewHolder {
        ImageView imageRes;
        TextView language;
        RadioButton radioBtn;

        public MyViewHolderDialogActivity(@NonNull View itemView) {
            super(itemView);
            imageRes = itemView.findViewById(R.id.imageRes);
            language = itemView.findViewById(R.id.language);
            radioBtn = itemView.findViewById(R.id.radioBtn);
            itemView.setOnClickListener(v -> {

            });
        }
    }
}
