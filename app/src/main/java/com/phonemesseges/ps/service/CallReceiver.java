package com.phonemesseges.ps.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.phonemesseges.ps.data.repository.SharedPrefferences.SaveLoadData;
import com.phonemesseges.ps.presentation.AppPref;

public class CallReceiver extends BroadcastReceiver {
    AppPref appPref;


    String outgoingCallNumber;

    @Override
    public void onReceive(Context context, Intent intent) {
        appPref = new AppPref(context);
        boolean isCallNotification = appPref.isCallNotification();
       // showToast(context, "isCallNotification: " + isCallNotification);
        if(isCallNotification) {
            Intent intent2;
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (TelephonyManager.EXTRA_STATE_RINGING.equals(state)) {
                TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                telephony.listen(new PhoneStateListener() {
                    @Override
                    public void onCallStateChanged(int state, String incomingNumber) {
                        super.onCallStateChanged(state, incomingNumber);

                        Log.d("MyTag", "входящий звонок: " + incomingNumber);
//                        showToast(context, "Входящий звонок");
                        Intent intent1;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            intent1 = new Intent(context, OverlayServiceOreo.class);
                            intent1.putExtra("incomingCallNumber", incomingNumber);
                            ContextCompat.startForegroundService(context, intent1);
                        } else {
                            intent1 = new Intent(context, OverlayServiceOreo.class);
                            intent1.putExtra("incomingCallNumber", incomingNumber);
                            context.startService(intent1);
                        }
                    }
                }, PhoneStateListener.LISTEN_CALL_STATE);

            } else if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(state)) {
                //} else if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
                outgoingCallNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                Log.d("MyTag", "исходящий звонок: " + outgoingCallNumber);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    intent2 = new Intent(context, OverlayServiceOreo.class);
                    intent2.putExtra("incomingCallNumber", outgoingCallNumber);
                    ContextCompat.startForegroundService(context, intent2);
                } else {
                    intent2 = new Intent(context, OverlayServiceOreo.class);
                    intent2.putExtra("incomingCallNumber", outgoingCallNumber);
                    context.startService(intent2);
                }

            }
            if (TelephonyManager.EXTRA_STATE_IDLE.equals(state)) {
                Log.d("MyTag", "Звонок завершён");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.stopService(new Intent(context, OverlayServiceOreo.class));
                } else {
                    context.stopService(new Intent(context, OverlayServiceOreo.class));
                }

            }

//        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
//            // Outgoing call
//            outgoingCallNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
//            Log.d("MyTag", "исходящий звонок: " + outgoingCallNumber);
//            Log.d("CLAppOut", outgoingCallNumber);
//            Intent intent2;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                intent2 = new Intent(context, OverlayServiceOreo.class);
//                intent2.putExtra("incomingCallNumber", outgoingCallNumber);
//                ContextCompat.startForegroundService(context, intent2);
//            } else {
//                intent2 = new Intent(context, OverlayService.class);
//                intent2.putExtra("incomingCallNumber", outgoingCallNumber);
//                context.startService(intent2);
//            }
//        }
//
//        if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
//            if (appPref.callNotification()) {
//                TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//                telephony.listen(new PhoneStateListener() {
//                    @Override
//                    public void onCallStateChanged(int state, String incomingNumber) {
//                        super.onCallStateChanged(state, incomingNumber);
//
//                        Log.d("MyTag", "входящий звонок");
////                        showToast(context, "Входящий звонок");
//                        Intent intent1;
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                            intent1 = new Intent(context, OverlayServiceOreo.class);
//                            intent1.putExtra("incomingCallNumber", incomingNumber);
//                            ContextCompat.startForegroundService(context, intent1);
//                        } else {
//                            intent1 = new Intent(context, OverlayService.class);
//                            intent1.putExtra("incomingCallNumber", incomingNumber);
//                            context.startService(intent1);
//                        }
//                    }
//                }, PhoneStateListener.LISTEN_CALL_STATE);
//
//                if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_IDLE)) {
//                    Log.d("MyTag", "завершение звонка");
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        context.stopService(new Intent(context, OverlayServiceOreo.class));
//                    } else {
//                        context.stopService(new Intent(context, OverlayService.class));
//                    }
////                showToast(context, "Звонок завершён");
//                }
//            }
//        } else {
//            Log.d("MyTag", "action.PHONE_STATE: false");
//
//        }
        }
    }

    void showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
