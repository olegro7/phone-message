package com.phonemesseges.ps.domain.interactor;

import com.phonemesseges.ps.data.call.PhoneNumberData;
import com.phonemesseges.ps.domain.repository.ICallRepository;

import java.util.ArrayList;

import io.reactivex.Observable;

public class PhoneCallInteractor implements InteractorInterface {
    private ICallRepository callRepository;

    public PhoneCallInteractor(ICallRepository callRepository) {
        this.callRepository = callRepository;
    }

    @Override
    public Observable<ArrayList<PhoneNumberData>> getCallLog() {
        return callRepository.getCallLog();
    }
}
