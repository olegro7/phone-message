package com.phonemesseges.ps.data.repository.SharedPrefferences;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.phonemesseges.ps.domain.repository.ISmsRepository;
import com.phonemesseges.ps.presentation.presenters.model.Enum.SmsLogModel;

import java.util.ArrayList;

import io.reactivex.Observable;

public class SmsRepository implements ISmsRepository {
    private Context context;

    public SmsRepository(Context context) {
        this.context = context;
    }

    @Override
    public Observable<ArrayList<SmsLogModel>> getSmsLog() {
        return getPhoneSms();
    }

    private Observable<ArrayList<SmsLogModel>> getPhoneSms() {
        Uri uriSms = Uri.parse("content://sms/inbox");
        Cursor cursor = context.getContentResolver().query(uriSms, new String[]{"_id", "address", "date", "body"},null,null,null);

        cursor.moveToFirst();
        ArrayList<SmsLogModel> phoneNumbers = new ArrayList<>();
        while (cursor.moveToNext()) {

            String contactName = getContactName(context, cursor.getString(1)) ;

            String address = cursor.getString(1);
            if (address.contains("+")) {
                StringBuilder sb = new StringBuilder(address);
                sb.deleteCharAt(0);
                address = sb.toString();
            }

            String date = cursor.getString(2);
            String body = cursor.getString(3);

            SmsLogModel smsLogModel = new SmsLogModel();

            smsLogModel.setContactName(contactName);
            smsLogModel.setPhoneNumber(address);
            smsLogModel.setSmsDate(date);
            smsLogModel.setSmsText(body);

            phoneNumbers.add(smsLogModel);
        }
        return Observable.fromArray(phoneNumbers);
    }

    public String getContactName(Context context, String phoneNumber) {
        if (!phoneNumber.contains("+")) {
            return phoneNumber;
        }
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        if (contactName == null || contactName.equals("")) {
            if (phoneNumber.contains("+")) {
                StringBuilder sb = new StringBuilder(phoneNumber);
                sb.deleteCharAt(0);
                phoneNumber = sb.toString();
            }
            return phoneNumber;
        }
        return contactName;
    }
}
