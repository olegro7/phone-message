package com.phonemesseges.ps.data.net.retrofit;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.phonemesseges.ps.BuildConfig;
import com.phonemesseges.ps.data.repository.SharedPrefferences.UniqueID;
import com.phonemesseges.ps.presentation.views.activities.MainActivity;

import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Web {
    private Context context;
    private UserClient userClient;
    public Web(Context context) {
        this.context = context;

        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
        okClientBuilder.connectTimeout(10, TimeUnit.SECONDS);
        okClientBuilder.readTimeout(10, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okClientBuilder.addInterceptor(interceptor);
        }

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://phoneblock.western-studio.ru/")
                .client(okClientBuilder.build())
               // .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        userClient = retrofit.create(UserClient.class);
    }
    public UserClient createRetrofit(){
        return userClient;
    }
}
