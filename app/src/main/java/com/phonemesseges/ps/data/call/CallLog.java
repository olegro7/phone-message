package com.phonemesseges.ps.data.call;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.reactivex.Observable;

public class CallLog {
    private Context context;

    public CallLog(Context context) {
        this.context = context;
    }

    public Observable<ArrayList<PhoneNumberData>> getCallLog() {
        ArrayList<PhoneNumberData> phoneNumberData = new ArrayList<>();

        Uri allCalls = Uri.parse("content://call_log/calls");
        Cursor c = context.getContentResolver().query(allCalls, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                String name = c.getString(c.getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME));// for name
//                Log.d("MyTag", name);
//                String duration = c.getString(c.getColumnIndex(CallLog.Calls.DURATION));// for duration
                String number = c.getString(c.getColumnIndex(android.provider.CallLog.Calls.NUMBER));// for  number
                if (number.contains("+")) {
                    StringBuilder sb = new StringBuilder(number);
                    sb.deleteCharAt(0);
                    number = sb.toString();
                }

                int columnIndex = c.getColumnIndex(android.provider.CallLog.Calls.DATE);// for  number
                long seconds = c.getLong(columnIndex);

//                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy" + " г., " + "HH:mm");
//                String phoneDate = sdf.format(new Date(seconds));

                int type = Integer.parseInt(c.getString(c.getColumnIndex(android.provider.CallLog.Calls.TYPE)));// for call type, Incoming or out going.

                PhoneNumberData pnd = new PhoneNumberData();
                pnd.setName(name);
                if (name == null || name.equals("")) pnd.setName(number);
                pnd.setNumber(number);
                pnd.setPhoneDate(String.valueOf(seconds));
                pnd.setType(type);

                phoneNumberData.add(pnd);
            } while (c.moveToNext());
        }
        c.close();
        return Observable.fromArray(phoneNumberData);
    }
}
