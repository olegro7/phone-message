package com.phonemesseges.ps.presentation.model;

public class ItemSpinner {
    private String title;
    private int res;

    public ItemSpinner(String title, int res) {
        this.res = res;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }


}
