package com.phonemesseges.ps.presentation.views.activities;

import android.support.design.button.MaterialButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse.GetCommentsResponse;
import com.phonemesseges.ps.data.repository.SharedPrefferences.SaveLoadData;
import com.phonemesseges.ps.presentation.presenters.model.Enum.CommentariesModel;
import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;
import com.phonemesseges.ps.presentation.views.adapters.RecyclerViewAdapter;
import com.phonemesseges.ps.presentation.views.dialogs.CommentariesDialog;
import com.phonemesseges.ps.service.App;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CommentariesActivity extends AppCompatActivity implements CommentariesDialog.CommentariesActivityCommunicationListener {
    private String phoneNumber;
    private String phoneName;

    String addToBL;
    String removeFromBL;

    boolean repeatNumber;

    TextView tvName;
    TextView tvNumber;
    TextView tvTitleText;
    TextView tvDate;
    MaterialButton btnBlackList;
    MaterialButton btnAddComment;
    ImageView imageWarning;

    ArrayList<Model> models;

    SwipeRefreshLayout refreshLayout;

    RecyclerView recyclerView;
    RecyclerViewAdapter rVAdapter;

    SaveLoadData saveLoadData;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commentaries);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        addToBL = getString(R.string.add_to_blacklist);
        removeFromBL = getString(R.string.remove_from_blacklist);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        saveLoadData = new SaveLoadData(this);
        gson = new Gson();

        models = new ArrayList<>();

        phoneNumber = getIntent().getStringExtra("phone");
        phoneName = getIntent().getStringExtra("name");

        tvName = findViewById(R.id.tvName);
        tvName.setText(phoneName);

        tvNumber = findViewById(R.id.tvNumber);
        tvNumber.setText(phoneNumber);

        tvTitleText = findViewById(R.id.tvTitleText);
        tvTitleText.setText(getString(R.string.span));

        tvDate = findViewById(R.id.tvDate);
        tvDate.setText(getIntent().getStringExtra("phoneDate"));

        btnBlackList = findViewById(R.id.btnBlackList);
        btnBlackList.setText(addToBL);
        if (saveLoadData.loadString(phoneNumber) != null) {
            btnBlackList.setText(saveLoadData.loadString(phoneNumber));
        }
        btnBlackList.setOnClickListener(v -> {
            if (btnBlackList.getText().toString().equals(addToBL)) {
                ArrayList<String> blackList;
                String blackListJSON = null;
                if (saveLoadData.loadString("blackList") != null) {
                    blackListJSON = saveLoadData.loadString("blackList");
                }
                if (blackListJSON != null && !blackListJSON.equals("") && !blackListJSON.equals("[]")) {
                    blackList = gson.fromJson(blackListJSON, new TypeToken<ArrayList<String>>(){}.getType());
                    for (String number : blackList) {
                        if (!phoneNumber.equals(number)) {
                            repeatNumber = true;
                        } else repeatNumber = false;
                    }
                    if (repeatNumber) {
                        blackList.add(phoneNumber);
                    }
                    String blackListTOJSON = gson.toJson(blackList);
                    saveLoadData.saveString("blackList", blackListTOJSON);
                } else {
                    blackList = new ArrayList<>();
                    blackList.add(phoneNumber);
                    String blackListTOJSON = gson.toJson(blackList);
                    saveLoadData.saveString("blackList", blackListTOJSON);
                }
                btnBlackList.setText(removeFromBL);
                saveLoadData.saveString(phoneNumber, removeFromBL);
            } else if (btnBlackList.getText().toString().equals(removeFromBL)) {
                ArrayList<String> blackList;
                if (saveLoadData.loadString("blackList") != null) {
                    blackList = new ArrayList<>(gson.fromJson(saveLoadData.loadString("blackList"), new TypeToken<ArrayList<String>>(){}.getType()));
                    for (String number : blackList) {
                        if (phoneNumber.equals(number)) {
//                            repeatNumber = true;
                            blackList.remove(phoneNumber);
                        }
                    }
                    String blackListTOJSON = gson.toJson(blackList);
                    saveLoadData.saveString("blackList", blackListTOJSON);
                }
                btnBlackList.setText(addToBL);
                saveLoadData.saveString(phoneNumber, addToBL);
            }
        });

        btnAddComment = findViewById(R.id.btnAddComment);
        btnAddComment.setText(getString(R.string.add_comment));
        btnAddComment.setOnClickListener(v -> {
            CommentariesDialog commentariesDialog = new CommentariesDialog();
            commentariesDialog.setCommentariesActivityCommunicationListener(this);

            Bundle bundle = new Bundle();
            bundle.putString("phoneNumber", phoneNumber);
            commentariesDialog.setArguments(bundle);

            commentariesDialog.show(getSupportFragmentManager(), "commentaries dialog");
        });

        refreshLayout = findViewById(R.id.refreshLayout);

        getCommentaries();

        refreshLayout.setOnRefreshListener(this::getCommentaries);

        imageWarning = findViewById(R.id.imageWarning);
        imageWarning.setImageResource(R.drawable.red);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        rVAdapter = new RecyclerViewAdapter(this, models);
        recyclerView.setAdapter(rVAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getCommentaries() {
        refreshLayout.setRefreshing(true);

        App app = (App) getApplication();
        app.getWebRepository().getComments(phoneNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetCommentsResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(GetCommentsResponse getCommentsResponse) {
                        models.clear();
                        rVAdapter.notifyDataSetChanged();

                        if (getCommentsResponse.data.comments == null || getCommentsResponse.data.comments.length == 0) {
                            Toast.makeText(CommentariesActivity.this, getString(R.string.no_comment), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        for (int i = 0; i < getCommentsResponse.data.comments.length; i++) {
                            CommentariesModel commentariesModel = new CommentariesModel();
                            commentariesModel.setDate(getCommentsResponse.data.comments[i].datetime_in_unix);
                            commentariesModel.setText(getCommentsResponse.data.comments[i].comment);
                            models.add(0, commentariesModel);
                        }
                        rVAdapter.notifyItemRangeInserted(0, models.size());
                    }

                    @Override
                    public void onError(Throwable e) {
                        refreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                        refreshLayout.setRefreshing(false);
                    }
                });
    }

    @Override
    public void refreshComments() {
        getCommentaries();
    }

}
