package com.phonemesseges.ps.presentation.presenters.model.Enum;

public class PhoneCallModel extends Model {
    private String number;
    private String date;
    private String titleText;
    private int warning;

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setWarning(int warning) {
        this.warning = warning;
    }

    public int getWarning() {
        return warning;
    }
}
