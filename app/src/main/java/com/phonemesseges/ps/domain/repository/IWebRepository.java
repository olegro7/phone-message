package com.phonemesseges.ps.domain.repository;

import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse.GetCommentsResponse;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponseList.GetCommentsResponseList;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse.RegistrationResponse;

import io.reactivex.Observable;
import retrofit2.http.Field;

public interface IWebRepository {
    Observable<RegistrationResponse> registration();
    Observable<RegistrationResponse> addComment(String phoneNumber, String comment, int status);
    Observable<GetCommentsResponse> getComments(String phone);
    Observable<GetCommentsResponseList> getLastCommentByPhones(String phones);
    Observable<RegistrationResponse> deletePhoneComments(String phone);
}
