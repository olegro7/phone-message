package com.phonemesseges.ps.presentation.contract;

import com.phonemesseges.ps.domain.repository.IWebRepository;

public interface MainActivityContract {
    public interface View {
        void setBeginningData();
        void showToast(String text);
    }

    public interface Presenter {
        void startView();
        void registration();
        void setRepository(IWebRepository repository);
        void attachView(MainActivityContract.View view);
        void detachView();
    }
}
