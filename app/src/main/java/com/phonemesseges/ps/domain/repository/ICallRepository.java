package com.phonemesseges.ps.domain.repository;

import com.phonemesseges.ps.data.call.PhoneNumberData;

import java.util.ArrayList;

import io.reactivex.Observable;

public interface ICallRepository {
    Observable<ArrayList<PhoneNumberData>> getCallLog();
}
