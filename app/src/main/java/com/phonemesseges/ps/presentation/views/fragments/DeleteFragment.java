package com.phonemesseges.ps.presentation.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse.RegistrationResponse;
import com.phonemesseges.ps.service.App;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DeleteFragment extends Fragment {
    View view;

    EditText inputNumberForDelete;
    MaterialButton btnDelete;

    public DeleteFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_delete, container, false);

        inputNumberForDelete = view.findViewById(R.id.inputNumberForDelete);

        btnDelete = view.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(v -> {
            String phoneNumber = inputNumberForDelete.getText().toString();
            if (phoneNumber.equals("")) {
                Toast.makeText(getContext(), getString(R.string.input_number), Toast.LENGTH_SHORT).show();
                return;
            }
            if (phoneNumber.contains("+")) {
                StringBuilder sb = new StringBuilder(inputNumberForDelete.getText().toString());
                sb.deleteCharAt(0);
                phoneNumber = sb.toString();
            }
            App app = (App) getActivity().getApplication();
            app.getWebRepository().deletePhoneComments(phoneNumber)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<RegistrationResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(RegistrationResponse registrationResponse) {
                            if (registrationResponse.error) {
                                Toast.makeText(getContext(), getString(R.string.no_number_db), Toast.LENGTH_SHORT).show();
                            } else {
                                inputNumberForDelete.setText("");
                                Toast.makeText(getContext(), getString(R.string.number_remove), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        });

        return view;
    }
}
