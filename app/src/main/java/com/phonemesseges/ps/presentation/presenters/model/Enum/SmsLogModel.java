package com.phonemesseges.ps.presentation.presenters.model.Enum;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SmsLogModel {
    private String contactName;
    private String phoneNumber;
    private String smsText;
    private String smsDate;

    public void setContactName(String phoneNumber) {
        this.contactName = phoneNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsDate(String smsDate) {
        long seconds = Long.parseLong(smsDate);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy" + " г., " + "HH:mm");
        this.smsDate = sdf.format(new Date(seconds));
    }

    public String getSmsDate() {
        return smsDate;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof SmsLogModel && (obj == this || ((SmsLogModel) obj).getPhoneNumber().equals(this.getPhoneNumber()));
    }
}
