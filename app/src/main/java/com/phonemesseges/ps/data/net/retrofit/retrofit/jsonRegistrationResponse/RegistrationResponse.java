package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegistrationResponse {
    @Expose
    @SerializedName("action")
    public String action;

    @Expose
    @SerializedName("error")
    public boolean error;

    @Expose
    @SerializedName("data")
    public DataRegistration data;

    @Expose
    @SerializedName("error_data")
    public ErrorArrayRegistration[] errorData;
}

