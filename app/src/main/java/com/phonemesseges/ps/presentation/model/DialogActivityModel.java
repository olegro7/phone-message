package com.phonemesseges.ps.presentation.model;

import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;

public class DialogActivityModel extends Model {
    private int imageRes;
    private String language;

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }
}
