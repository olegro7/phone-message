package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponseList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentsList {
    @Expose
    @SerializedName("phone")
    public String phone;

    @Expose
    @SerializedName("comment")
    public String comment;

    @Expose
    @SerializedName("datetime_in_unix")
    public int datetime_in_unix;
}
