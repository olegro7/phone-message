package com.phonemesseges.ps.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.phonemesseges.ps.data.repository.SharedPrefferences.SaveLoadData;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class IncomingCallReceiver extends BroadcastReceiver {
    SaveLoadData saveLoadData;
    Gson gson;

    boolean numberForBlocking = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        saveLoadData = new SaveLoadData(context);
        gson = new Gson();

        ITelephony telephonyService;
        try {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            if (number != null) {
                if (number.contains("+")) {
                    StringBuilder sb = new StringBuilder(number);
                    sb.deleteCharAt(0);
                    number = sb.toString();
                }
            }


            if(state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)){
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                try {
                    Method m = tm.getClass().getDeclaredMethod("getITelephony");

                    m.setAccessible(true);
                    telephonyService = (ITelephony) m.invoke(tm);

                    String blackListFROMJson = saveLoadData.loadString("blackList");
                    ArrayList<String> blackList = gson.fromJson(blackListFROMJson, new TypeToken<ArrayList<String>>(){}.getType());

                    if (blackListFROMJson != null && !blackListFROMJson.equals("") && !blackListFROMJson.equals("[]")) {
                        for (String num : blackList) {
                            if (num.equals(number)) {
                                numberForBlocking = true;
                            } else {
                                numberForBlocking = false;
                            }
                        }
                    }

                    if ((number != null) && numberForBlocking) {
                        telephonyService.endCall();
//                        Toast.makeText(context, "Ending the call from: " + number, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Toast.makeText(context, "Ring " + number, Toast.LENGTH_SHORT).show();

            }
            if(state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)){
//                Toast.makeText(context, "Answered " + number, Toast.LENGTH_SHORT).show();
            }
            if(state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_IDLE)){
//                Toast.makeText(context, "Idle "+ number, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
