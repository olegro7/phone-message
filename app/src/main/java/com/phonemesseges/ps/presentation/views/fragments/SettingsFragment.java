package com.phonemesseges.ps.presentation.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.repository.SharedPrefferences.SaveLoadData;
import com.phonemesseges.ps.presentation.views.activities.DialogActivity;
import com.phonemesseges.ps.utility.LocaleHelper;

public class SettingsFragment extends android.support.v7.preference.PreferenceFragmentCompat {
    PreferenceCategory preferenceCategory;
    SwitchPreference switchPreference;
    PreferenceScreen preferenceScreen;

    ActivityCommunicationListener activityCommunication;

    SaveLoadData saveLoadData;

    public void setActivityCommunicationListener(ActivityCommunicationListener activityCommunication) {
        this.activityCommunication = activityCommunication;
    }

    public interface ActivityCommunicationListener {
        void refreshActivity();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
//        setPreferencesFromResource(R.xml.fragment_settings, rootKey);
        addPreferencesFromResource(R.xml.fragment_settings);
        saveLoadData = new SaveLoadData(getContext());

        preferenceCategory = (PreferenceCategory) getPreferenceManager().findPreference("settings");
        preferenceCategory.setTitle(getString(R.string.settings));

        switchPreference = (SwitchPreference) getPreferenceManager().findPreference("call_notification");
        switchPreference.setTitle(getString(R.string.turn_on_notification));

        preferenceScreen = (PreferenceScreen) getPreferenceManager().findPreference("language");
        preferenceScreen.setIntent(new Intent(getContext(), DialogActivity.class));
        preferenceScreen.setTitle(getString(R.string.choose_language));

        if (saveLoadData.loadString("icon_flag") == null) {
            preferenceScreen.setIcon(R.drawable.russ_icon);
            LocaleHelper.setLocale(getContext(), "ru");
        } else if (saveLoadData.loadString("icon_flag") != null) {
            String flag = saveLoadData.loadString("icon_flag");
            switch (flag) {
                case "ru":
                    preferenceScreen.setIcon(R.drawable.russ_icon);
                    break;
                case "en":
                    preferenceScreen.setIcon(R.drawable.en_icon);
                    break;
                case "sp":
                    preferenceScreen.setIcon(R.drawable.sp_icon);
                    break;
            }
        }
//
//        String entryValue = preferenceScreen.getValue();
//        if (entryValue == null) {
//            preferenceScreen.setValueIndex(0);
//            preferenceScreen.setIcon(R.drawable.russ_icon);
//            LocaleHelper.setLocale(getContext(), "ru");
//
////            activityCommunication.refreshActivity();
////            getActivity().getSupportFragmentManager().beginTransaction().detach(this).commitNowAllowingStateLoss();
////            getActivity().getSupportFragmentManager().beginTransaction().attach(this).commitAllowingStateLoss();
//        }
//
//        preferenceScreen.setOnPreferenceChangeListener((preference, newValue) -> {
//            String userSelectedValue = (String) newValue;
//            switch (userSelectedValue) {
//                case "ru":
//                    saveLoadData.saveString("icon_flag", "ru");
//                    break;
//                case "en":
//                    saveLoadData.saveString("icon_flag", "en");
//                    break;
//                case "sp":
//                    saveLoadData.saveString("icon_flag", "sp");
//                    break;
//            }
//            LocaleHelper.setLocale(getContext(), userSelectedValue);
//
//            activityCommunication.refreshActivity();
//            getActivity().getSupportFragmentManager().beginTransaction().detach(this).commitNowAllowingStateLoss();
//            getActivity().getSupportFragmentManager().beginTransaction().attach(this).commitAllowingStateLoss();
//            return true;
//        });
    }
}
