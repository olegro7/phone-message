package com.phonemesseges.ps.presentation.views.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse.RegistrationResponse;
import com.phonemesseges.ps.service.App;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CommentariesDialog extends AppCompatDialogFragment implements View.OnClickListener {
    private int charactersLimit = 245;

    int redWarning = 1;
//    int yellowWarning = 2;
//    int greenWarning = 3;

    //    По умолчанию поставил 1
    int warningStatus = redWarning;

    private float alpha = 0.3f;

    TextView tvNumber;
    TextView tvText;

    ImageButton btnRedWarning;
    ImageButton btnYellowWarning;
    ImageButton btnGreenWarning;

    EditText editText;
    TextView tvCharactersLimit;

    MaterialButton btnAdd;

    CommentariesActivityCommunicationListener commentariesActivityCommunicationListener;

    public interface CommentariesActivityCommunicationListener {
        void refreshComments();
    }

    public void setCommentariesActivityCommunicationListener(CommentariesActivityCommunicationListener commentariesActivityCommunicationListener) {
        this.commentariesActivityCommunicationListener = commentariesActivityCommunicationListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_comment_dialog, null);

        view.findViewById(R.id.btnClose).setOnClickListener(v -> dismiss());

        tvNumber = view.findViewById(R.id.tvName);
        tvNumber.setText(getArguments().getString("phoneNumber"));

        tvText = view.findViewById(R.id.tvText);
        tvText.setText(getString(R.string.add_comment_for_number));

        btnRedWarning = view.findViewById(R.id.btnRedWarning);
        btnRedWarning.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.red));
//        btnRedWarning.setOnClickListener(this);
//        btnRedWarning.setClickable(false);

//        btnYellowWarning = view.findViewById(R.id.btnYellowWarning);
//        btnYellowWarning.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.yellow));
//        btnYellowWarning.setOnClickListener(this);
//        btnYellowWarning.setAlpha(alpha);
//
//        btnGreenWarning = view.findViewById(R.id.btnGreenWarning);
//        btnGreenWarning.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.green));
//        btnGreenWarning.setOnClickListener(this);
//        btnGreenWarning.setAlpha(alpha);

        tvCharactersLimit = view.findViewById(R.id.tvCharactersLimit);

        editText = view.findViewById(R.id.editText);
        editText.setHint(getString(R.string.write_comment));
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setCharactersQuantity(editText.length());
            }
        });

        btnAdd = view.findViewById(R.id.btnAdd);
        btnAdd.setText(getString(R.string.add));
        btnAdd.setOnClickListener(v -> {
            if (editText.getText().length() == 0) {
                Toast.makeText(getContext(), getString(R.string.input_comment), Toast.LENGTH_SHORT).show();
            } else {
                String number = tvNumber.getText().toString();
                String comment = editText.getText().toString();
                App app = (App) getActivity().getApplication();
                app.getWebRepository()
                        .addComment(number, comment, warningStatus)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<RegistrationResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(RegistrationResponse registrationResponse) {
//                            if (registrationResponse.error) {
//                                Toast.makeText(getContext(), registrationResponse.errorData[0].errorText, Toast.LENGTH_SHORT).show();
//                                Log.d("MyTag", registrationResponse.errorData[0].errorText);
//                            } else {
//                                Toast.makeText(getContext(), registrationResponse.data.tokenKey, Toast.LENGTH_SHORT).show();
//                                Log.d("MyTag", registrationResponse.data.tokenKey);
//                            }
                            }

                            @Override
                            public void onError(Throwable e) {
                                dismiss();
                            }

                            @Override
                            public void onComplete() {
                                if (commentariesActivityCommunicationListener != null) {
                                    commentariesActivityCommunicationListener.refreshComments();
                                }
                                dismiss();
                            }
                        });
            }
        });

        return builder.setView(view).create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRedWarning:
                btnYellowWarning.setAlpha(alpha);
                btnGreenWarning.setAlpha(alpha);
                btnRedWarning.setAlpha(1.0f);
                warningStatus = redWarning;
                break;
//            case R.id.btnYellowWarning:
//                btnRedWarning.setAlpha(alpha);
//                btnGreenWarning.setAlpha(alpha);
//                btnYellowWarning.setAlpha(1.0f);
//                warningStatus = yellowWarning;
//                break;
//            case R.id.btnGreenWarning:
//                btnRedWarning.setAlpha(alpha);
//                btnYellowWarning.setAlpha(alpha);
//                btnGreenWarning.setAlpha(1.0f);
//                warningStatus = greenWarning;
//                break;
        }
    }

    @SuppressLint("SetTextI18n")
    public void setCharactersQuantity(int length) {
        int quantity = charactersLimit - length;
        tvCharactersLimit.setText(getString(R.string.left) + " " + quantity + " " + getString(R.string.characters));
    }

    public interface CommentariesDialogCommunication {
        void sendData(String phoneNumber, String comment);
    }
}
