package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponseList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorArrayGetCommentsList {
    @Expose
    @SerializedName("code")
    public String code;

    @Expose
    @SerializedName("error_text")
    public String errorText;
}
