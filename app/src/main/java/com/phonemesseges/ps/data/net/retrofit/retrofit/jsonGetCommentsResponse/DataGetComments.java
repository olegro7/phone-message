package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataGetComments {
    @Expose
    @SerializedName("phone")
    public String phone;

    @Expose
    @SerializedName("comments")
    public Comments[] comments;
}
