package com.phonemesseges.ps.domain.repository;

import com.phonemesseges.ps.data.call.PhoneNumberData;
import com.phonemesseges.ps.presentation.presenters.model.Enum.SmsLogModel;

import java.util.ArrayList;

import io.reactivex.Observable;

public interface ISmsRepository {
    Observable<ArrayList<SmsLogModel>> getSmsLog();
}
