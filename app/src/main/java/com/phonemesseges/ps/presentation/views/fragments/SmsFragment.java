package com.phonemesseges.ps.presentation.views.fragments;

import android.arch.paging.PagedList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.repository.SharedPrefferences.SmsRepository;
import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;
import com.phonemesseges.ps.presentation.presenters.model.Enum.SmsModel;
import com.phonemesseges.ps.presentation.views.adapters.RecyclerViewAdapter;
import com.phonemesseges.ps.presentation.views.adapters.paging.MainThreadExecutor;
import com.phonemesseges.ps.presentation.views.adapters.smsLog.PagingSmsLogRVAdapter;
import com.phonemesseges.ps.presentation.views.adapters.smsLog.SmsLogPositionDataSource;
import com.phonemesseges.ps.service.App;

import java.util.ArrayList;
import java.util.concurrent.Executors;

public class SmsFragment extends Fragment {
    RecyclerView recyclerView;
    RecyclerViewAdapter rVAdapter;

    PagingSmsLogRVAdapter pagingRVAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sms, container, false);

        SmsLogPositionDataSource dataSource = new SmsLogPositionDataSource(new SmsRepository(getContext()), ((App)getActivity().getApplication()).getWebRepository());

        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(5)
                .build();

        PagedList pagedList = new PagedList.Builder<>(dataSource, config)
                .setNotifyExecutor(new MainThreadExecutor())
                .setFetchExecutor(Executors.newFixedThreadPool(2))
                .build();

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        pagingRVAdapter = new PagingSmsLogRVAdapter(getContext());
        pagingRVAdapter.submitList(pagedList);
        rVAdapter = new RecyclerViewAdapter(getActivity(), getSmsDates());

        recyclerView.setAdapter(pagingRVAdapter);

        return view;
    }

    private ArrayList<Model> getSmsDates() {
        ArrayList<Model> models = new ArrayList<>();
        int a = 0;
        for (int i = 0; i < 10; i++) {
            SmsModel smsModel = new SmsModel();
            smsModel.setName("+7 927 3509157");
            smsModel.setTitleText("Спам или мошенничество");
            smsModel.setDate("01.11.2018 г., 15:32");
            smsModel.setSmsText("Глубокий уровень погружения способствует подготовке и реализации вывода текущих активов. Равным образом, новая модель организационной деятельности, в своем классическом представлении, допускает внедрение");
            switch (a) {
                case 0:
                    smsModel.setWarning(R.drawable.green);
                    break;
                case 1:
                    smsModel.setWarning(R.drawable.yellow);
                    break;
                case 2:
                    smsModel.setWarning(R.drawable.red);
                    a = 0;
                    break;
            }
            a++;
            models.add(smsModel);
        }
        return models;
    }}
