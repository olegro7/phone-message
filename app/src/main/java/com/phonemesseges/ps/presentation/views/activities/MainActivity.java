package com.phonemesseges.ps.presentation.views.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.repository.SharedPrefferences.SaveLoadData;
import com.phonemesseges.ps.domain.interactor.InteractorInterface;
import com.phonemesseges.ps.presentation.contract.MainActivityContract;
import com.phonemesseges.ps.presentation.presenters.MainActivityPresenter;
import com.phonemesseges.ps.presentation.views.adapters.ViewPagerAdapter;
import com.phonemesseges.ps.presentation.views.fragments.CommentariesFragment;
import com.phonemesseges.ps.presentation.views.fragments.DeleteFragment;
import com.phonemesseges.ps.presentation.views.fragments.PhoneCallFragment;
import com.phonemesseges.ps.presentation.views.fragments.SearchFragment;
import com.phonemesseges.ps.presentation.views.fragments.SettingsFragment;
import com.phonemesseges.ps.presentation.views.fragments.SmsFragment;
import com.phonemesseges.ps.service.App;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View, DialogActivity.ActivityCommunicationListener {
    final int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 9899;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private InteractorInterface interactor;

    ViewPagerAdapter adapter;

    MainActivityContract.Presenter activityPresenter;

    PhoneCallFragment phoneCallFragment;
    SmsFragment smsFragment;
    CommentariesFragment commentariesFragment;
    SettingsFragment settingsFragment;
    SearchFragment searchFragment;
    DeleteFragment deleteFragment;

    SaveLoadData saveLoadData;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        saveLoadData = new SaveLoadData(this);

        activityPresenter = new MainActivityPresenter(this);
        activityPresenter.attachView(this);
        activityPresenter.startView();

//        WebRepository webRepository = new WebRepository(this);
//        webRepository.getLastCommentByPhones("{\"1\":\"380951673659\"}")
//                .subscribe(new Observer<GetCommentsResponse>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onNext(GetCommentsResponse getCommentsResponse) {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });

    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE) {
            if (!Settings.canDrawOverlays(this)) {
                // You don't have permission
                checkPermission();
            }
        }
    }

    @Override
    public void setBeginningData() {
        checkPermission();

        activityPresenter.setRepository(((App) getApplication()).getWebRepository());

//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        if (!prefs.getBoolean("firstTime", false)) {
//            // <---- run your one time code here
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
//                activityPresenter.registration();
////            // mark first time has ran.
//            SharedPreferences.Editor editor = prefs.edit();
//            editor.putBoolean("firstTime", true);
//            editor.apply();
//        }

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        phoneCallFragment = new PhoneCallFragment();
        smsFragment = new SmsFragment();
//        commentariesFragment = new CommentariesFragment();
        settingsFragment = new SettingsFragment();

        searchFragment = new SearchFragment();
        deleteFragment = new DeleteFragment();

        boolean b = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED;
        boolean b1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED;
        boolean b2 = ContextCompat.checkSelfPermission(this, Manifest.permission.SYSTEM_ALERT_WINDOW) != PackageManager.PERMISSION_GRANTED;
        boolean b3 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED;
        boolean b4 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED;
        boolean b5 = ContextCompat.checkSelfPermission(this, Manifest.permission.PROCESS_OUTGOING_CALLS) != PackageManager.PERMISSION_GRANTED;
        boolean b6 = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED;
        boolean b7 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED;
        if (b1 && b && b2 && b3 && b4 && b5 && b6 && b7) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_CALL_LOG,
                            Manifest.permission.SYSTEM_ALERT_WINDOW,
                            Manifest.permission.READ_SMS,
                            Manifest.permission.READ_PHONE_NUMBERS,
                            Manifest.permission.PROCESS_OUTGOING_CALLS,
                            Manifest.permission.CALL_PHONE,
                            Manifest.permission.READ_CONTACTS}, 1);

        } else {
            activityPresenter.registration();
            setFragments();
        }
    }

    private void setFragments() {
        adapter.addFragment(phoneCallFragment);
        adapter.addFragment(smsFragment);
//        adapter.addFragment(commentariesFragment);
        adapter.addFragment(settingsFragment);
        adapter.addFragment(searchFragment);
        adapter.addFragment(deleteFragment);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        setIconForTabs();

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getIcon() != null) {
                    tab.getIcon().setColorFilter(Color.parseColor("#a6d6e6"), PorterDuff.Mode.SRC_IN);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getIcon() != null) {
                    tab.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length == 8) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    // && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED
                    && grantResults[4] == PackageManager.PERMISSION_GRANTED
                    && grantResults[5] == PackageManager.PERMISSION_GRANTED
                    && grantResults[6] == PackageManager.PERMISSION_GRANTED
                    && grantResults[7] == PackageManager.PERMISSION_GRANTED) {

                activityPresenter.registration();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    setFragments();
                }
            }

            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, getString(R.string.info_permission_call_status), Toast.LENGTH_SHORT).show();
            }
            if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, getString(R.string.info_permission_call_list), Toast.LENGTH_SHORT).show();
            }
//            if (grantResults[2] == PackageManager.PERMISSION_DENIED) {
//                Toast.makeText(this, "Разрешите использовать SYSTEM ALERT WINDOW", Toast.LENGTH_SHORT).show();
//            }
            if (grantResults[3] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, getString(R.string.info_permission_sms), Toast.LENGTH_SHORT).show();
            }
            if (grantResults[4] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, getString(R.string.info_permission_phone_number), Toast.LENGTH_SHORT).show();
            }
            if (grantResults[5] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, getString(R.string.info_permission_call_status), Toast.LENGTH_SHORT).show();
            }
            if (grantResults[6] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, getString(R.string.info_permission_call_phone), Toast.LENGTH_SHORT).show();
            }
            if (grantResults[7] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, getString(R.string.info_permission_phone_contacts), Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void refreshActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    public void setIconForTabs() {
        int[] tabIcons = {
                R.drawable.phonecall,
                R.drawable.sms,
//                R.drawable.commentaries,
                R.drawable.settings,
                R.drawable.search,
                R.drawable.delete};
        for (int i = 0; i < 5; i++) {
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
            if (i == 0)
                tabLayout.getTabAt(i).getIcon().setColorFilter(Color.parseColor("#a6d6e6"), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Reload", "onResume");
        if (saveLoadData.loadInt("reloadActivity") == 1) {
            saveLoadData.saveInt("reloadActivity", 0);
            refreshActivity();
        }
    }
}
