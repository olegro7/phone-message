package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse.DataRegistration;

public class GetCommentsResponse {
    @Expose
    @SerializedName("action")
    public String action;

    @Expose
    @SerializedName("error")
    public boolean error;

    @Expose
    @SerializedName("data")
    public DataGetComments data;

    @Expose
    @SerializedName("error_data")
    public ErrorArrayGetComments[] errorData;
}
