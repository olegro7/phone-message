package com.phonemesseges.ps.presentation.views.activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.repository.SharedPrefferences.SaveLoadData;
import com.phonemesseges.ps.presentation.model.DialogActivityModel;
import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;
import com.phonemesseges.ps.presentation.views.adapters.RecyclerViewAdapter;
import com.phonemesseges.ps.presentation.views.fragments.SettingsFragment;
import com.phonemesseges.ps.utility.LocaleHelper;

import java.util.ArrayList;

public class DialogActivity extends Activity {

    RecyclerView recyclerView;
    RecyclerViewAdapter rVAdapter;

    SaveLoadData saveLoadData;

    RadioGroup radioGroup;

    RadioButton rb1;
    RadioButton rb2;
    RadioButton rb3;

    ActivityCommunicationListener activityCommunication;

    public void setActivityCommunicationListener(ActivityCommunicationListener activityCommunication) {
        this.activityCommunication = activityCommunication;
    }

    public interface ActivityCommunicationListener {
        void refreshActivity();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        this.setFinishOnTouchOutside(true);
        saveLoadData = new SaveLoadData(this);

        radioGroup = findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = findViewById(checkedId);
            rb.setOnClickListener(v -> {
                saveLoadData.saveInt("reloadActivity", 1);
                finish();
            });
            switch (rb.getId()) {
                case R.id.rb1:
                    saveLoadData.saveString("icon_flag", "ru");
                    LocaleHelper.setLocale(this, "ru");
                    break;
                case R.id.rb2:
                    saveLoadData.saveString("icon_flag", "en");
                    LocaleHelper.setLocale(this, "en");
                    break;
                case R.id.rb3:
                    saveLoadData.saveString("icon_flag", "sp");
                    LocaleHelper.setLocale(this, "sp");
                    break;
            }
        });

        rb1 = findViewById(R.id.rb1);
        rb2 = findViewById(R.id.rb2);
        rb3 = findViewById(R.id.rb3);

        if (saveLoadData.loadString("icon_flag") == null) {
            rb1.setChecked(true);
        } else if (saveLoadData.loadString("icon_flag").equals("ru")) {
            rb1.setChecked(true);
        } else if (saveLoadData.loadString("icon_flag").equals("en")) {
            rb2.setChecked(true);
        } else if (saveLoadData.loadString("icon_flag").equals("sp")) {
            rb3.setChecked(true);
        }

//        recyclerView = findViewById(R.id.recyclerView);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        rVAdapter = new RecyclerViewAdapter(this, getData());
//        recyclerView.setAdapter(rVAdapter);
    }

    private ArrayList<Model> getData() {
        String[] languages = {"Русский", "English", "Español"};
        int[] flags = {R.drawable.russ_icon, R.drawable.en_icon, R.drawable.sp_icon};

        ArrayList<Model> languagesList = new ArrayList();
        for (int i = 0; i < languages.length; i++) {
            DialogActivityModel model = new DialogActivityModel();
            model.setLanguage(languages[i]);
            model.setImageRes(flags[i]);
            languagesList.add(model);
        }
        return languagesList;
    }
}
