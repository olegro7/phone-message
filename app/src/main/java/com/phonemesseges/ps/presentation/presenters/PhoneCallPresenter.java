package com.phonemesseges.ps.presentation.presenters;

import android.provider.CallLog;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.call.PhoneNumberData;
import com.phonemesseges.ps.domain.interactor.InteractorInterface;
import com.phonemesseges.ps.presentation.contract.PhoneCallContract;
import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;
import com.phonemesseges.ps.presentation.presenters.model.Enum.PhoneCallModel;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class PhoneCallPresenter implements PhoneCallContract.Presenter {
    private PhoneCallContract.View view;
    private InteractorInterface interactor;

    @Override
    public void setInteractor(InteractorInterface interactor) {
        this.interactor = interactor;
    }

    @Override
    public void getCallLog() {
        interactor.getCallLog().subscribe(new Observer<ArrayList<PhoneNumberData>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<PhoneNumberData> value) {
                ArrayList<Model> callLog = new ArrayList<>();
                for (int i = 0; i < value.size(); i++) {
                    PhoneCallModel phoneCallModel = new PhoneCallModel();
                    phoneCallModel.setNumber(value.get(i).getNumber());
                    phoneCallModel.setTitleText("");
                    phoneCallModel.setDate(value.get(i).getPhoneDate());
                    phoneCallModel.setWarning(R.drawable.red);
                    callLog.add(phoneCallModel);
                }

                view.setBeginningData(callLog);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void attachView(PhoneCallContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }
}
