package com.phonemesseges.ps.presentation.presenters.model.Enum;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommentariesModel extends Model {
    private String text;
    private String date;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setDate(long dateInUnix) {
        Date date = new Date(dateInUnix*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy");
        String s = sdf.format(date);
        this.date = s;
        Log.d("DateTime", s);
    }

    public String getDate() {
        return date;
    }
}
