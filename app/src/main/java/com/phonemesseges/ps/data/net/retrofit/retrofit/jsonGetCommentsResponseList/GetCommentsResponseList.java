package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponseList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCommentsResponseList {
    @Expose
    @SerializedName("action")
    public String action;

    @Expose
    @SerializedName("error")
    public boolean error;

    @Expose
    @SerializedName("data")
    public DataGetCommentsList data;

    @Expose
    @SerializedName("error_data")
    public ErrorArrayGetCommentsList[] errorData;
}
