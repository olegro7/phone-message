package com.phonemesseges.ps.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse.GetCommentsResponse;
import com.phonemesseges.ps.data.repository.SharedPrefferences.SaveLoadData;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.phonemesseges.ps.service.App.CHANNEL_ID;

public class OverlayServiceOreo extends Service {
    private final String keyX = "keyX";
    private final String keyY = "keyY";

    int initialX;
    int initialY;
    float initialTouchX;
    float initialTouchY;

    private WindowManager windowManager;
    private View layout;

    private Notification notification;
    private WindowManager.LayoutParams params;
    private LayoutInflater li;

    private SaveLoadData save_loadData;
    private boolean isShowView = false;
    private String phoneNumber;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate() {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    // very important, this sends touch events to underlying views
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    // very important, this sends touch events to underlying views
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }
    }

    @SuppressLint({"CheckResult", "ClickableViewAccessibility"})
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String number = intent.getStringExtra("incomingCallNumber");
        if (phoneNumber == null || !phoneNumber.equals(number)) {
            phoneNumber = number;
            addView(phoneNumber, params);
        } else {
            return START_NOT_STICKY;
        }
        save_loadData = new SaveLoadData(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle(getString(R.string.caller))
                    .build();
            startForeground(1, notification);
        }


        // Layout gravity
//        params.gravity = Gravity.CENTER;

        params.x = save_loadData.loadInt(keyX) != -1 ? save_loadData.loadInt(keyX) : 0;
        params.y = save_loadData.loadInt(keyY) != -1 ? save_loadData.loadInt(keyY) : -130;

        LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Your overlay layout
        layout = li.inflate(R.layout.overlay_notification, null);
        layout.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    initialX = params.x;
                    initialY = params.y;
                    initialTouchX = event.getRawX();
                    initialTouchY = event.getRawY();
                    return true;
                case MotionEvent.ACTION_MOVE:
                    // Обрабатываем позицию касания и обноваляем размер Layout'а
                    params.x = initialX + (int) (event.getRawX() - initialTouchX);
                    params.y = initialY + (int) (event.getRawY() - initialTouchY);
                    windowManager.updateViewLayout(layout, params);

                    savePosition(params.x, params.y);
                    return true;
            }
            return false;
        });

        // Layout gravity
//        params.gravity = Gravity.CENTER;

        params.x = save_loadData.loadInt(keyX) != -1 ? save_loadData.loadInt(keyX) : 0;
        params.y = save_loadData.loadInt(keyY) != -1 ? save_loadData.loadInt(keyY) : -130;


        li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // Your overlay layout
        layout = li.inflate(R.layout.overlay_notification, null);

        // Your overlay layout
        layout.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    initialX = params.x;
                    initialY = params.y;
                    initialTouchX = event.getRawX();
                    initialTouchY = event.getRawY();
                case MotionEvent.ACTION_MOVE:
                    // Обрабатываем позицию касания и обноваляем размер Layout'а
                    params.x = initialX + (int) (event.getRawX() - initialTouchX);
                    params.y = initialY + (int) (event.getRawY() - initialTouchY);
                    windowManager.updateViewLayout(layout, params);

                    savePosition(params.x, params.y);
                    return true;
            }
            return false;
        });


        return START_NOT_STICKY;
    }

    private void addView(String number, WindowManager.LayoutParams params) {
        App app = (App) getApplication();
        app.getWebRepository().getComments(number)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetCommentsResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(GetCommentsResponse getCommentsResponse) {
                        TextView tvMessage = layout.findViewById(R.id.tvMessage);
                        ((ImageView) layout.findViewById(R.id.ivIcon)).setImageResource(R.drawable.red);

                        if (getCommentsResponse.data.comments == null || getCommentsResponse.data.comments.length == 0 || getCommentsResponse.data.comments[0].comment.equals("")) {
//                            tvMessage.setText(getString(R.string.no_comment));
                            stopService(new Intent(getApplicationContext(), OverlayService.class));
                        } else {
                            String comment = getCommentsResponse.data.comments[0].comment;
                            tvMessage.setText(comment);
                            windowManager.addView(layout, params);
                            isShowView = true;
                            ((Button) layout.findViewById(R.id.btnClose)).setOnClickListener(v -> {
                                windowManager.removeView(layout);
                                layout = null;
                                isShowView = false;
                                Log.d("MyTag", "stopService");
                                stopService(new Intent(getApplicationContext(), OverlayService.class));
                            });
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void savePosition(int x, int y) {
        save_loadData.saveInt(keyX, x);
        save_loadData.saveInt(keyY, y);
    }

    @Override
    public void onDestroy() {
        if (layout != null) {
            try {
                windowManager.removeViewImmediate(layout);
            } catch (Exception e) {
            }

        }
        super.onDestroy();

    }
}
