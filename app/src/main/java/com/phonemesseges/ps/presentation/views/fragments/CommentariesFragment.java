package com.phonemesseges.ps.presentation.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.data.repository.SharedPrefferences.SaveLoadData;
import com.phonemesseges.ps.presentation.presenters.model.Enum.CommentariesModel;
import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;
import com.phonemesseges.ps.presentation.views.adapters.RecyclerViewAdapter;

import java.util.ArrayList;

public class CommentariesFragment extends Fragment {
    TextView tvNumber;
    TextView tvTitleText;
    TextView tvDate;
    MaterialButton btnBlackList;
    MaterialButton addComment;
    ImageView imageWarning;

    RecyclerView recyclerView;
    RecyclerViewAdapter rVAdapter;

    SaveLoadData saveLoadData;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_commentaries, container, false);
        saveLoadData = new SaveLoadData(getContext());

        tvNumber = view.findViewById(R.id.tvName);
        //tvNumber.setText("+7 927 3509157");

        tvTitleText = view.findViewById(R.id.tvTitleText);
       // tvTitleText.setText("Спам или мошенничество");

        tvDate = view.findViewById(R.id.tvDate);
      //  tvDate.setText("01.11.2018 г., 15:32");

        btnBlackList = view.findViewById(R.id.btnBlackList);
        btnBlackList.setOnClickListener(v -> {
            if (btnBlackList.getText().toString().equals(getString(R.string.add_to_blacklist))) {
                btnBlackList.setText(getString(R.string.remove_from_blacklist));

            } else if (btnBlackList.getText().toString().equals(getString(R.string.remove_from_blacklist))) {
                btnBlackList.setText(getString(R.string.add_to_blacklist));
            }
            //Toast.makeText(getActivity(), "Button works", Toast.LENGTH_SHORT).show();
        });

        addComment = view.findViewById(R.id.addComment);
        addComment.setText(getString(R.string.add_comment));
        addComment.setOnClickListener(v -> {

        });

        imageWarning = view.findViewById(R.id.imageWarning);
        imageWarning.setImageResource(R.drawable.red);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rVAdapter = new RecyclerViewAdapter(getActivity(), getCommentariesDates());
        recyclerView.setAdapter(rVAdapter);

        return view;
    }

    private ArrayList<Model> getCommentariesDates() {
        ArrayList<Model> models = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            CommentariesModel commentariesModel = new CommentariesModel();
            commentariesModel.setDate(6546513);
            commentariesModel.setText("Не берите трубку. Мошенники! Если перезвонить - снимают деньги со счёта. Не берите трубку. Мошенники!");
            models.add(commentariesModel);
        }
        return models;
    }
}
