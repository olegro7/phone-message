package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataRegistration {
    @Expose
    @SerializedName("token_key")
    public String tokenKey;

    @Expose
    @SerializedName("is_created")
    public boolean isCreated;
}
