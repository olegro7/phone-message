package com.phonemesseges.ps.presentation.contract;

import android.content.Context;

import com.phonemesseges.ps.data.call.PhoneNumberData;
import com.phonemesseges.ps.domain.interactor.InteractorInterface;
import com.phonemesseges.ps.presentation.presenters.model.Enum.Model;

import java.util.ArrayList;

public interface PhoneCallContract {
    public interface View {
        void setBeginningData(ArrayList<Model> callLog);
    }

    public interface Presenter {
        void getCallLog();
        void setInteractor(InteractorInterface interactor);
        void attachView(PhoneCallContract.View view);
        void detachView();
    }
}
