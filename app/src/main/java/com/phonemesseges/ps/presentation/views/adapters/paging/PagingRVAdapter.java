package com.phonemesseges.ps.presentation.views.adapters.paging;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.presentation.model.CallPhoneInfo;
import com.phonemesseges.ps.presentation.views.activities.CommentariesActivity;

public class PagingRVAdapter extends PagedListAdapter<CallPhoneInfo, PagingRVAdapter.CallInfoHolder> {
    private Context context;

    public PagingRVAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    private DialogListener dialogListener;

    public interface DialogListener {
        void onClickAdd(String phoneNumber);
    }

    public void setDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    private static DiffUtil.ItemCallback<CallPhoneInfo> DIFF_CALLBACK = new DiffUtil.ItemCallback<CallPhoneInfo>() {
        @Override
        public boolean areItemsTheSame(@NonNull CallPhoneInfo oldItemPosition, @NonNull CallPhoneInfo newItemPosition) {
            return oldItemPosition.getPhoneComment().equals(newItemPosition.getPhoneComment());
        }

        @Override
        public boolean areContentsTheSame(@NonNull CallPhoneInfo oldItem, @NonNull CallPhoneInfo newItem) {
            return oldItem.equals(newItem);
        }
    };

    @NonNull
    @Override
    public CallInfoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_phonecall, viewGroup, false);
        return new CallInfoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallInfoHolder callInfoHolder, int i) {
        callInfoHolder.tvName.setText(getItem(i).getPhoneName());
        callInfoHolder.tvTitleText.setText(getItem(i).getPhoneComment());
        if (callInfoHolder.tvTitleText.getText().toString().equals("")) {
            callInfoHolder.tvTitleText.setVisibility(View.GONE);
        }
        callInfoHolder.tvDate.setText(getItem(i).getDate());
        callInfoHolder.imageWarning.setImageResource(R.drawable.red);
    }

    public class CallInfoHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvTitleText;
        TextView tvDate;
        ImageView imageWarning;
        ImageButton btnAddCommentaries;

        public CallInfoHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, CommentariesActivity.class);
                intent.putExtra("phone", getItem(getAdapterPosition()).getPhoneNumber());
                intent.putExtra("name", getItem(getAdapterPosition()).getPhoneName());
                intent.putExtra("phoneDate", getItem(getAdapterPosition()).getDate());
                context.startActivity(intent);
            });
            tvName = itemView.findViewById(R.id.name);
            tvTitleText = itemView.findViewById(R.id.titleText);
            tvDate = itemView.findViewById(R.id.date);
            imageWarning = itemView.findViewById(R.id.warning);
            btnAddCommentaries = itemView.findViewById(R.id.btnAddCommentaries);
            btnAddCommentaries.setOnClickListener(v -> dialogListener.onClickAdd(getItem(getAdapterPosition()).getPhoneNumber()));
        }
    }
}
