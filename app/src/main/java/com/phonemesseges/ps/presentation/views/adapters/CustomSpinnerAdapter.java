package com.phonemesseges.ps.presentation.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.presentation.model.ItemSpinner;

import java.util.List;

public class CustomSpinnerAdapter extends BaseAdapter {
    private static class ViewHolder {
        ImageView icon;
        TextView title;
    }
    List<ItemSpinner> list;
    public CustomSpinnerAdapter(List<ItemSpinner> list){
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ItemSpinner getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) parent.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_spinner, parent, false);
            mViewHolder.icon = (ImageView) convertView.findViewById(R.id.icon);
            mViewHolder.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.icon.setImageResource(getItem(position).getRes());
        mViewHolder.title.setText(getItem(position).getTitle());

        return convertView;    }
}
