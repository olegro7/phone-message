package com.phonemesseges.ps.data.repository.SharedPrefferences;

import android.content.Context;
import android.util.Log;

import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse.GetCommentsResponse;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponseList.GetCommentsResponseList;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse.RegistrationResponse;
import com.phonemesseges.ps.data.net.retrofit.UserClient;
import com.phonemesseges.ps.data.net.retrofit.Web;
import com.phonemesseges.ps.domain.repository.IWebRepository;
import com.phonemesseges.ps.utility.Utility;

import java.util.Random;

import io.reactivex.Observable;

public class WebRepository implements IWebRepository {
    private Context context;

    private SaveLoadData saveLoadData;
    private UserClient client;

    private final String TOKEN_KEY = "token_key";

    public WebRepository(Context context) {
        this.context = context;
        saveLoadData = new SaveLoadData(context);
        client = new Web(context).createRetrofit();
    }

    @Override
    public Observable<RegistrationResponse> registration() {
        return numberRegistration(client);
    }

    private Observable<RegistrationResponse> numberRegistration(UserClient client) {
        String uniqueID = Utility.getUniqueID(context);
        String number = Utility.getPhoneNumber(context);
        Observable<RegistrationResponse> registrationResponse;
        if (number == null || number.equals("")) {
            registrationResponse = client.numberRegistration(uniqueID, getSaltString());
        } else {
            registrationResponse = client.numberRegistration(uniqueID, number);
        }

        return registrationResponse
                .flatMap(response -> {
                    if (response.error) {
                        Log.d("MyTag", response.errorData[0].errorText + ": " + response.errorData[0].code);
                        return client.numberAuthorization(uniqueID, number);
                    } else
                        return Observable.fromCallable(() -> response);
                }).doOnNext(reg -> saveToken(reg.data.tokenKey));
    }

    private String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 14) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }

    @Override
    public Observable<RegistrationResponse> addComment(String phoneNumber, String comment, int status) {
        return client.addComment(loadToken(), phoneNumber, comment, status);
    }

    @Override
    public Observable<GetCommentsResponse> getComments(String phone) {
        String tokenKey = loadToken();
        return client.getComments(tokenKey, phone);
    }
    @Override

    public Observable<GetCommentsResponseList> getLastCommentByPhones(String phones) {
        return client.getLastCommentByPhones(loadToken(), phones);
    }

    @Override
    public Observable<RegistrationResponse> deletePhoneComments(String phone) {
        return client.deletePhoneComments(loadToken(), phone);
    }

    private void saveToken(String token) {
        saveLoadData.saveString(TOKEN_KEY, token);
    }

    private String loadToken() {
        return saveLoadData.loadString(TOKEN_KEY);
    }

}
