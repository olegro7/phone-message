package com.phonemesseges.ps.presentation.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.phonemesseges.ps.R;
import com.phonemesseges.ps.presentation.model.ItemSpinner;
import com.phonemesseges.ps.presentation.views.activities.CommentariesActivity;
import com.phonemesseges.ps.presentation.views.adapters.CustomSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {
    View view;

    EditText inputNumber;
    MaterialButton btnSearch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);

        inputNumber = view.findViewById(R.id.inputNumber);
//        Spinner spinner = view.findViewById(R.id.spinner);
//        List list = new ArrayList();
//        list.add(new ItemSpinner( "+7", R.drawable.rus_icon));
//        list.add(new ItemSpinner( "+7", R.drawable.en_icon));
//        list.add(new ItemSpinner( "+7", R.drawable.rus_icon));
//        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter()
        btnSearch = view.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(v -> {
            if (inputNumber.getText().toString().isEmpty()) {
                Toast.makeText(getContext(), getString(R.string.input_number), Toast.LENGTH_SHORT).show();
                return;
            }
            Intent intent = new Intent(getContext(), CommentariesActivity.class);
            intent.putExtra("phone", inputNumber.getText().toString());
            startActivity(intent);
//            Intent intent = new Intent(getContext(), DialogActivity.class);
//            startActivity(intent);

        });

        return view;
    }
}
