package com.phonemesseges.ps.utility;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.phonemesseges.ps.data.repository.SharedPrefferences.UniqueID;

public class Utility {

    public static String getPhoneNumber(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            SubscriptionManager subManager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            SubscriptionInfo activeSubscriptionInfo = subManager.getActiveSubscriptionInfo(0);
            String number = activeSubscriptionInfo != null ? activeSubscriptionInfo.getNumber(): "testNumber";
            if(number == null) number = "testNumber";
            return number;
        }

        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        return tMgr.getLine1Number();
    }

    public static String getUniqueID(Context context) { return new UniqueID().getID(context); }

    public static String getNumbersJson(String[] numbers) {
        StringBuilder json = new StringBuilder("{");
        for (int i = 0; i < numbers.length; i++) {
            String number = numbers[i].replace("+", "");
            if (i == numbers.length - 1)
                json.append("\"").append(i + 1).append("\":\"").append(number).append("\"");
            else json.append("\"").append(i + 1).append("\":\"").append(number).append("\",");

        }
        json.append("}");
        return json.toString();
    }
}
