package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comments {
    @Expose
    @SerializedName("comment")
    public String comment;

    @Expose
    @SerializedName("datetime_in_unix")
    public int datetime_in_unix;
}
