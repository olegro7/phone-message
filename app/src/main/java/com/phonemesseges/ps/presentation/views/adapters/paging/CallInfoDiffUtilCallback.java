package com.phonemesseges.ps.presentation.views.adapters.paging;

import android.support.v7.util.DiffUtil;

import com.phonemesseges.ps.presentation.model.CallPhoneInfo;

import java.util.List;

public class CallInfoDiffUtilCallback extends DiffUtil.Callback {

    private final List<CallPhoneInfo> oldList;
    private final List<CallPhoneInfo> newList;

    public CallInfoDiffUtilCallback(List<CallPhoneInfo> oldList, List<CallPhoneInfo> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        CallPhoneInfo oldCallPhoneInfo = oldList.get(oldItemPosition);
        CallPhoneInfo newCallPhoneInfo = newList.get(newItemPosition);
        return oldCallPhoneInfo.getPhoneComment().equals(newCallPhoneInfo.getPhoneComment());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        CallPhoneInfo oldCallPhoneInfo = oldList.get(oldItemPosition);
        CallPhoneInfo newCallPhoneInfo = newList.get(newItemPosition);
        return oldCallPhoneInfo.getPhoneComment().equals(newCallPhoneInfo.getPhoneComment())
                && oldCallPhoneInfo.getPhoneNumber().equals(newCallPhoneInfo.getPhoneNumber());
    }
}
