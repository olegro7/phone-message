package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class ErrorArrayGetComments {
    @Expose
    @SerializedName("code")
    public String code;

    @Expose
    @SerializedName("error_text")
    public String errorText;
}
