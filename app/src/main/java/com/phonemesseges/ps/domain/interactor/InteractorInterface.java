package com.phonemesseges.ps.domain.interactor;

import com.phonemesseges.ps.data.call.PhoneNumberData;

import java.util.ArrayList;

import io.reactivex.Observable;

public interface InteractorInterface {
    Observable<ArrayList<PhoneNumberData>> getCallLog();
}
