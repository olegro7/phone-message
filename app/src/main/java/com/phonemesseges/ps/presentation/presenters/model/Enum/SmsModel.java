package com.phonemesseges.ps.presentation.presenters.model.Enum;

public class SmsModel extends Model {
    private String name;
    private String date;
    private String titleText;
    private String smsText;
    private int warning;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setWarning(int warning) {
        this.warning = warning;
    }

    public int getWarning() {
        return warning;
    }
}
