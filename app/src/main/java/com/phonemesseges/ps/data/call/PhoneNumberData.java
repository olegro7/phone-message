package com.phonemesseges.ps.data.call;

public class PhoneNumberData {
    private String name;
    private String number;
    private String phoneDate;
    private int type;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setPhoneDate(String phoneDate) {
        this.phoneDate = phoneDate;
    }

    public String getPhoneDate() {
        return phoneDate;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
