package com.phonemesseges.ps.presentation.views.adapters.paging;

import android.annotation.SuppressLint;
import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;

import com.phonemesseges.ps.data.call.PhoneNumberData;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponse.GetCommentsResponse;
import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonGetCommentsResponseList.GetCommentsResponseList;
import com.phonemesseges.ps.domain.repository.ICallRepository;
import com.phonemesseges.ps.domain.repository.ISmsRepository;
import com.phonemesseges.ps.domain.repository.IWebRepository;
import com.phonemesseges.ps.presentation.model.CallInfo;
import com.phonemesseges.ps.presentation.model.CallPhoneInfo;
import com.phonemesseges.ps.utility.Utility;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class PhoneLogPositionalDataSource extends PositionalDataSource {

//    ArrayList<PhoneNumberData> phoneNumberData;

    private List<CallInfo> phoneContacts;

    private ICallRepository callRepository;
    private IWebRepository webRepository;

    public PhoneLogPositionalDataSource(ICallRepository callRepository, IWebRepository webRepository) {
        this.callRepository = callRepository;
        this.webRepository = webRepository;
    }

    public interface CallBack {
        void setCallBack(List<CallPhoneInfo> callInfo);
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback callback) {
        callRepository.getCallLog().subscribe(new Observer<ArrayList<PhoneNumberData>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @SuppressLint("CheckResult")
            @Override
            public void onNext(ArrayList<PhoneNumberData> phoneData) {
                Collections.sort(phoneData, (a, b) -> Long.valueOf(a.getPhoneDate()).compareTo(Long.valueOf(b.getPhoneDate())));
                Collections.reverse(phoneData);
                phoneContacts = new ArrayList<>();
                for (int i = 0; i < phoneData.size(); i++) {
                    CallInfo callInfo = new CallInfo();
                    callInfo.setName(phoneData.get(i).getName());
                    callInfo.setNumber(phoneData.get(i).getNumber());
                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy" + " г., " + "HH:mm");
                    String phoneDate = sdf.format(new Date(Long.valueOf(phoneData.get(i).getPhoneDate())));
                    callInfo.setDate(phoneDate);
                    phoneContacts.add(callInfo);
                }
                int endPosition = params.requestedStartPosition + params.requestedLoadSize;
                endPosition = endPosition >= phoneContacts.size() ? phoneContacts.size() : endPosition;
                List<CallInfo> partOfContacts = phoneContacts.subList(params.requestedStartPosition, endPosition);

                loadPhoneData(callInfo -> callback.onResult(callInfo, 0), partOfContacts);

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback callback) {
        if (params.startPosition >= phoneContacts.size())
            return;
        int endPosition = params.startPosition + params.loadSize;
        endPosition = endPosition >= phoneContacts.size() ? phoneContacts.size() - 1 : endPosition;
        List<CallInfo> partOfContacts = phoneContacts.subList(params.startPosition, endPosition);

        loadPhoneData(callInfo -> callback.onResult(callInfo), partOfContacts);
    }

    @SuppressLint("CheckResult")
    private void loadPhoneData(CallBack callBack, List<CallInfo> partOfContacts) {
        Observable.fromArray(partOfContacts)
                .flatMap(new Function<List<CallInfo>, Observable<GetCommentsResponseList>>() {
                    @Override
                    public Observable<GetCommentsResponseList> apply(List<CallInfo> listCallInfo) throws Exception {
                        List<String> numbers = new ArrayList<>();
                        for (CallInfo item : listCallInfo) {
                            if (item.getNumber() != null && !item.getNumber().isEmpty())
                                numbers.add(item.getNumber());
                        }
                        String[] array = numbers.toArray(new String[numbers.size()]);
                        return webRepository.getLastCommentByPhones(Utility.getNumbersJson(array));
                    }
                }, new BiFunction<List<CallInfo>, GetCommentsResponseList, List<CallPhoneInfo>>() {
                    @Override
                    public List<CallPhoneInfo> apply(List<CallInfo> phoneInfo, GetCommentsResponseList commentsResponse) throws Exception {
                        List<CallPhoneInfo> data = new ArrayList<>();
                        for (CallInfo item : phoneInfo) {
                            CallPhoneInfo callPhoneInfo = new CallPhoneInfo();
                            callPhoneInfo.setPhoneName(item.getName());
                            callPhoneInfo.setPhoneNumber(item.getNumber());
                            callPhoneInfo.setDate(item.getDate());
                            if (commentsResponse.data.comments == null || commentsResponse.data.comments.length == 0) {
                                callPhoneInfo.setPhoneComment("");
                            } else {
                                String phoneComment = null;
                                for (int i = 0; i < commentsResponse.data.comments.length; i++) {
                                    if (item.getNumber().equals(commentsResponse.data.comments[i].phone)) {
                                        phoneComment = commentsResponse.data.comments[i].comment;
                                    }
                                }
                                callPhoneInfo.setPhoneComment(phoneComment);
                            }
                            data.add(callPhoneInfo);
                        }

                        return data;
                    }
                })

                //.toSortedList(new StringDateComparator())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(callBack::setCallBack, Throwable::printStackTrace);
    }


    class StringDateComparator implements Comparator<CallInfo> {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.getDefault());

        public int compare(CallInfo o1, CallInfo o2) {
            try {
                String data1 = o1.getDate().replace(" г.,", "");
                String data2 = o2.getDate().replace(" г.,", "");
                return -dateFormat.parse(data1).compareTo(dateFormat.parse(data2));
            } catch (ParseException e) {
                e.printStackTrace();

            }
            return 0;
        }
    }
}
