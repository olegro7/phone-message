package com.phonemesseges.ps.data.repository.SharedPrefferences;

import android.content.Context;

import com.phonemesseges.ps.data.call.CallLog;
import com.phonemesseges.ps.data.call.PhoneNumberData;
import com.phonemesseges.ps.domain.repository.ICallRepository;

import java.util.ArrayList;

import io.reactivex.Observable;

public class CallRepository implements ICallRepository {
    private Context context;

    public CallRepository(Context context) {
        this.context = context;
    }

    public Observable<ArrayList<PhoneNumberData>> getCallLog() {
        return new CallLog(context).getCallLog();
    }
}
