package com.phonemesseges.ps.presentation.presenters;

import android.content.Context;
import android.util.Log;

import com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse.RegistrationResponse;
import com.phonemesseges.ps.domain.repository.IWebRepository;
import com.phonemesseges.ps.presentation.contract.MainActivityContract;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivityPresenter implements MainActivityContract.Presenter {
    private Context context;

    public MainActivityPresenter(Context context) {
        this.context = context;
    }

    private MainActivityContract.View view;
    private IWebRepository repository;

    @Override
    public void startView() {
        view.setBeginningData();
    }

    @Override
    public void registration() {
        repository.registration()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RegistrationResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
                
            }

            @Override
            public void onNext(RegistrationResponse value) {
                if (value.error) {
                    view.showToast(value.errorData[0].errorText);
                    Log.d("MyTag", value.errorData[0].errorText);
                } else {
                    view.showToast(value.data.tokenKey);
                    Log.d("MyTag", value.data.tokenKey);
                }

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void setRepository(IWebRepository repository) {
        this.repository = repository;
    }

    @Override
    public void attachView(MainActivityContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }
}
