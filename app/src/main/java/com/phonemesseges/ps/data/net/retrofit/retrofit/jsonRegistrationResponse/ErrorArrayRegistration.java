package com.phonemesseges.ps.data.net.retrofit.retrofit.jsonRegistrationResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorArrayRegistration {
    @Expose
    @SerializedName("code")
    public String code;

    @Expose
    @SerializedName("error_text")
    public String errorText;
}
